/* Longest Collatz Sequence
 *
 * Finds the longest collatz sequence in numbers from [0, 1,000,000]
 *
 * Auther: Jeremy Tobac
 * Date: October 2014
 */
#include <stdio.h>
#include <stdlib.h>

/**@brief Perform collatz test on a number
 *
 * If the number is odd: 3*n+1
 * If the number is even: n/2
 *
 *@param unsigned int uintStart: Number to perform collatzTest
 *
 *@return Total amount of numbers found by performing the collatz test
 */
int collatzTest(unsigned int uintStart)
{
	int intTotal = 0;

	while(uintStart > 1){
		if(uintStart % 2 == 0){
			uintStart = uintStart / 2;
			intTotal++;
		}
		else{
			uintStart = uintStart * 3 + 1;
			intTotal++;
		}
	}

	intTotal++;
	
	return intTotal;
	
}

int main()
{
	unsigned int uintTest = 0;
	int intTotal = 0;
	int intMaxTotal = 0;
	unsigned int uintMax = 0;

	for(uintTest = 0; uintTest < 1000000; uintTest++){
		intTotal = collatzTest(uintTest);
		printf("Tested %u, Total %d\n", uintTest, intTotal);
		if(intMaxTotal < intTotal){
			intMaxTotal = intTotal;
			uintMax = uintTest;
		}
		printf("Current Max %u, Max Length %d\n", uintMax, intMaxTotal);
	}
	
	return 0;
}
